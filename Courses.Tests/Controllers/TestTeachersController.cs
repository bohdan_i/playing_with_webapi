﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Courses.Tests.Helpers;
using Courses.Models;
using Courses.Controllers;
using System.Web.Http.Results;
using System.Threading.Tasks;
using System.Net;

namespace Courses.Tests.Controllers
{
    /// <summary>
    /// Testing TestTeachersController using TestAppDbContext
    /// </summary>
    [TestClass]
    public class TestTeachersController
    {
        public TestTeachersController()
        {
            AutoMapperConfig.Configure();
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        [TestMethod]
        public async Task PostTeacher_ShouldReturnSameTeacher()
        {
            var controller = new TeachersController(new TestAppDbContext());

            var item = GetDemoTeacher();

            var result =
                 await controller.PostTeacher(item) as CreatedAtRouteNegotiatedContentResult<DTOs.TeacherDTO>;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteName, "DefaultApi");
            Assert.AreEqual(result.RouteValues["id"], result.Content.TeacherId);
            Assert.AreEqual(result.Content.TeacherName, item.TeacherName);
        }

        [TestMethod]
        public async Task PutTeacher_ShouldReturnStatusCode()
        {
            var controller = new TeachersController(new TestAppDbContext());

            var item = GetDemoTeacher();

            var result = await controller.PutTeacher(item.TeacherId, item) as StatusCodeResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(StatusCodeResult));
            Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
        }

        [TestMethod]
        public async Task PutTeacher_ShouldFail_WhenDifferentID()
        {
            var controller = new TeachersController(new TestAppDbContext());

            var badresult = await controller.PutTeacher(999, GetDemoTeacher());
            Assert.IsInstanceOfType(badresult, typeof(BadRequestResult));
        }

        // This test fails with AutoMapper 6.1.1 but passes with 6.0.2 (?)
        [TestMethod]
        public async Task GetTeacher_ShoulReturnTeacherWithSameID()
        {
            var context = new TestAppDbContext();
            var teacher = GetDemoTeacher();
            context.Teachers.Add(teacher);

            var controller = new TeachersController(context);
            var result = await controller.GetTeacher(2) as OkNegotiatedContentResult<DTOs.TeacherDetailsDTO>;

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Content.TeacherId);
        }

        [TestMethod]
        public void GetTeachers_ShouldReturnAllTeachers()
        {
            var context = new TestAppDbContext();
            context.Teachers.Add(new Teacher { TeacherId = 1, TeacherName = "Teacher1" });
            context.Teachers.Add(new Teacher { TeacherId = 2, TeacherName = "Teacher2" });
            context.Teachers.Add(new Teacher { TeacherId = 3, TeacherName = "Teacher3" });

            var controller = new TeachersController(context);
            var result = controller.GetTeachers() as List<DTOs.TeacherDTO>;

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
        }

        [TestMethod]
        public async Task DeleteTeacher_ShouldReturnOK()
        {
            var context = new TestAppDbContext();
            var item = GetDemoTeacher();
            context.Teachers.Add(item);

            var controller = new TeachersController(context);
            var result = await controller.DeleteTeacher(2) as OkNegotiatedContentResult<Teacher>;

            Assert.IsNotNull(result);
            Assert.AreEqual(item.TeacherId, result.Content.TeacherId);
        }

        Teacher GetDemoTeacher()
        {
            return new Teacher { TeacherId = 2, TeacherName = "Teacher2" };
        }
    }
}
