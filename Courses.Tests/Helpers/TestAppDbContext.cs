﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Courses.Models;

namespace Courses.Tests.Helpers
{
    public class TestAppDbContext : IApplicationDbContext
    {
        public TestAppDbContext()
        {
            Courses = new TestCourseDbSet();
            Teachers = new TestTeacherDbSet();
            Students = new TestStudentDbSet();
        }

        public DbSet<Teacher> Teachers { get; set; }

        public DbSet<Course> Courses { get; set; }

        public DbSet<Student> Students { get; set; }

        public int SaveChanges()
        {
            return 0;
        }

        public void MarkAsModified(object item) { }
        public void Dispose() { }

        public async Task<int> SaveChangesAsync()
        {
            return 0;
        }
        
    }
}