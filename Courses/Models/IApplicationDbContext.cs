﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Courses.Models
{
    public interface IApplicationDbContext : IDisposable
    {
        DbSet<Teacher> Teachers { get; }
        DbSet<Course> Courses { get; }
        DbSet<Student> Students { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
        void MarkAsModified(object item);
    }
}
