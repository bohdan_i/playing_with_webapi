﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Courses.Models
{
    public class Teacher
    {
        public Teacher()
        {
            this.Courses = new HashSet<Course>();
        }

        public int TeacherId { get; set; }
        [Required]
        public string TeacherName { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        //public int GetCount()
        //{
        //    return Courses.Count;
        //}
    }
}