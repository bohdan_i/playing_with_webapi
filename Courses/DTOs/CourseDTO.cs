﻿namespace Courses.DTOs
{
    public class CourseDTO
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public int StudentCount { get; set; }
    }
}