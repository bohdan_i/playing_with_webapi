﻿using System.Collections.Generic;

namespace Courses.DTOs
{
    public class TeacherDetailsDTO
    {
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public IEnumerable<CourseDTO> Courses { get; set; }
    }
}