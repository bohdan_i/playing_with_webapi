﻿namespace Courses.DTOs
{
    public class TeacherDTO
    {
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public int CoursesCount { get; set; }
    }
}