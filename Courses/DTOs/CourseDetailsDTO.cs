﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Courses.DTOs
{
    public class CourseDetailsDTO
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }

        public IEnumerable<StudentDTO> Students { get; set; }
        public TeacherDTO Teacher { get; set; }
    }
}