﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Courses.DTOs
{
    public class StudentDetailsDTO
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }

        public ICollection<CourseDTO> Courses { get; set; }
    }
}