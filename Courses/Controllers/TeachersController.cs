﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Courses.Models;
using Courses.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace Courses.Controllers
{
    public class TeachersController : ApiController
    {
        private IApplicationDbContext db = new ApplicationDbContext();

        public TeachersController() { }

        public TeachersController(IApplicationDbContext context)
        {
            db = context;
        }

        // GET: api/Teachers
        public List<TeacherDTO> GetTeachers()
        {
            return db.Teachers.ProjectTo<TeacherDTO>().ToList();
        }

        // GET: api/Teachers/5
        [ResponseType(typeof(TeacherDetailsDTO))]
        public async Task<IHttpActionResult> GetTeacher(int id)
        {
            var teacher = await db.Teachers.ProjectTo<TeacherDetailsDTO>().SingleOrDefaultAsync(t => t.TeacherId == id);
            if (teacher == null)
            {
                return NotFound();
            }

            return Ok(teacher);
        }

        // PUT: api/Teachers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTeacher(int id, Teacher teacher)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != teacher.TeacherId)
            {
                return BadRequest();
            }

            db.MarkAsModified(teacher);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TeacherExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Teachers
        [ResponseType(typeof(TeacherDTO))]
        public async Task<IHttpActionResult> PostTeacher(Teacher teacher)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Teachers.Add(teacher);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = teacher.TeacherId }, Mapper.Map<Teacher, TeacherDTO>(teacher));
        }

        // DELETE: api/Teachers/5
        [ResponseType(typeof(Teacher))]
        public async Task<IHttpActionResult> DeleteTeacher(int id)
        {
            Teacher teacher = await db.Teachers.FindAsync(id);
            if (teacher == null)
            {
                return NotFound();
            }

            db.Teachers.Remove(teacher);
            await db.SaveChangesAsync();

            return Ok(teacher);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TeacherExists(int id)
        {
            return db.Teachers.Count(e => e.TeacherId == id) > 0;
        }
    }
}