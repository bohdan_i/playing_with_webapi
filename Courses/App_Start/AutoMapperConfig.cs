﻿using AutoMapper;
using Courses.Models;
using Courses.DTOs;

namespace Courses
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Teacher, TeacherDTO>()
                    .ForMember(dto => dto.CoursesCount, conf => conf.MapFrom(ol => ol.Courses.Count));
                cfg.CreateMap<Course, CourseDTO>()
                    .ForMember(dto => dto.StudentCount, conf => conf.MapFrom(ol => ol.Students.Count));
                cfg.CreateMap<Student, StudentDTO>()
                    .ForMember(dto => dto.CoursesCount, conf => conf.MapFrom(ol => ol.Courses.Count));
                cfg.CreateMap<Teacher, TeacherDetailsDTO>();
                cfg.CreateMap<Course, CourseDetailsDTO>();
            });
        }
    }
}