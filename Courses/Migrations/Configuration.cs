namespace Courses.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Courses.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Courses.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Courses.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            Teacher teacher1 = new Teacher { TeacherId = 1, TeacherName = "Grand Master Yoda" };
            Teacher teacher2 = new Teacher { TeacherId = 2, TeacherName = "Obi-Wan Kenobi" };

            Student student1 = new Student { StudentId = 1, StudentName = "Luke Skywalker" };
            Student student2 = new Student { StudentId = 2, StudentName = "Anakin Skywalker" };

            Course course1 = new Course { CourseId = 1, CourseName = "Physics" };
            Course course2 = new Course { CourseId = 1, CourseName = "Maths" };
            Course course3 = new Course { CourseId = 1, CourseName = "Test course do not attend" };

            student1.Courses.Add(course1);
            student1.Courses.Add(course2);
            student2.Courses.Add(course2);
            student2.Courses.Add(course3);
            teacher1.Courses.Add(course1);
            teacher1.Courses.Add(course3);
            teacher2.Courses.Add(course2);

            context.Teachers.AddOrUpdate(
                t => t.TeacherId,
                teacher1,
                teacher2
            );

            context.Students.AddOrUpdate(
                s => s.StudentId,
                student1,
                student2
            );

            context.Courses.AddOrUpdate(
                c => c.CourseId,
                course1,
                course2,
                course3
            );
            
        }
    }
}
